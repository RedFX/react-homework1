import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/scss/style.scss';

import { Navbar } from '../../../client/Navbar/components/Navbar';

export const App = () => {
    return (
        <Navbar/>
    );
};
