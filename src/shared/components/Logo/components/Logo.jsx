import React from 'react';

const location = {
    header: 'header-logo',
    footer: 'footer-logo',
    copyright: 'copyright-logo'
}

export const Logo = ({where}) => {
    return (
        <div className={`logo ${location[where]}`}>Movie<span>Rise</span></div>
    );
};