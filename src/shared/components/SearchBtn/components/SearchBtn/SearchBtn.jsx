import React from 'react';
import SearchImg from '../../../../../assets/images/icon/Search_Icon.png';

export const SearchBtn = (size) => {
    return (
        <a href="#" className="search-btn">
            <img className="search-icon" height='14px' width='14px' src={SearchImg} alt="Search"/>
        </a>
    );
};