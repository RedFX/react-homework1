import React from 'react';

const typeButton = {
    main: 'primary',
    second: 'second',
}

export const Button = ({text, type}) => {
    return (
    <button className={`btn ${typeButton[type]}`}>{text}</button>
    );
} ;