import React from 'react';
import {Logo} from "../../../shared/components/Logo/components/Logo";
import {Button} from "../../../shared/components/Button/components/Button";
import {SearchBtn} from "../../../shared/components/SearchBtn/components/SearchBtn"

export const Navbar = () => {
    return (
        <navbar className="navbar">
            <div className="container">
                <div className="row">
                    <div className="col-md-4 d-flex align-items-center">
                        <Logo where="header"/>
                    </div>
                    <div className="col-md-8 d-flex justify-content-end align-items-center">
                        <SearchBtn />
                        <Button text="Sign in"/>
                        <Button text="Sign Up" type="main"/>
                    </div>
                </div>
        </div>
        </navbar>
    );
};